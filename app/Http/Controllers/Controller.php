<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class Controller extends BaseController
{
    //
    protected function respondWithToken($token)
    {
        $logging = 'Login System';
        return response()->json([
            'success' => true,
            'errors' =>[],
            'warnings' => [],
            'data' => array(
                'accessToken' => $token,
                'user' => Auth::user()
            ),
            'dictionaries' => [],
            'Log' => $this->logging($logging),
        ], 200);
    }

    protected function logging($action)
    {
        // Store Log
        // Store Log - Define Object
        $arrayLogging = array(
            "user_id"=>Auth::user()->id,
            "user_name"=>Auth::user()->name,
            "user_email"=>Auth::user()->email,
            "user_departement"=>Auth::user()->departement,
            "user_division"=>Auth::user()->division,
            "action"=>$action,
            "time"=>Carbon::now('Asia/Jakarta')->toDateTimeString(),
        );

        $getData = Storage::disk('local')->get('logging.json');
        $getDataDecode = json_decode($getData,true);
        $arrayLoggingEncode= json_encode($arrayLogging);
        array_push($getDataDecode, $arrayLogging);

        // Store Log - Define Object
        $encodeNewData = json_encode($getDataDecode);
        Storage::disk('local')->put('logging.json', $encodeNewData);
         
        return $arrayLogging;
    }


    protected function paging($data)
    {
         // Handle Response
         $response['paging']['orderBy'] = $data[0];
         $response['paging']['sortBy'] = $data[1];
         $response['paging']['pageRequest'] = (int)$data[2]->currentPage();
         $response['paging']['sizeRequest'] = (int)$data[2]->perPage();
         $response['paging']['totalElement'] = (int)$data[2]->total();
         $response['paging']['totalPage'] = (int)$data[2]->lastPage();
         $paging = $response;
         
        return $paging;
    }
}
