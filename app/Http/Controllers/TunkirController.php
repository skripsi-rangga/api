<?php

namespace App\Http\Controllers;
use App\Models\Period;
use App\Models\Detail;
use App\Models\Pegawai;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class TunkirController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Get Master Data.
     *
     * @return Response
     */


    public function listRequest(Request $request)
    {

        // Action Name For Logging:
        $logging='Access Master Data List';
        
        // Request Data
        $name= $request->input('name');
        $nip= $request->input('nip');
        $size= $request->input('size')  ? $request->input('size') : 10;
        $orderBy = $request->input('orderBy') ? $request->input('orderBy') : 'created_at';
        $sortBy = $request->input('sortBy') ? $request->input('sortBy') : 'ASC';
        $period = $request->input('period');

        // Query
        $listData = Detail::select('detail.*', 'pegawai.nama', 'pegawai.jabatan', 'pegawai.grade', 'pegawai.status', 'period.start_period', 'period.end_period',  'period.year')
        ->leftjoin('pegawai','pegawai.nip','=','detail.nip')
        ->leftjoin('period','period.uuid','=','detail.periode')
        ->where('pegawai.nama','Like','%'.$name.'%')
        ->where('detail.nip','Like','%'.$nip.'%')
        ->where('detail.periode','Like','%'.$period.'%')
        ->orderBy($orderBy, $sortBy)->paginate($size);
        $datapaging= array($orderBy, $sortBy, $listData);


        return response()->json([
            'success' => true,
            'errors' =>[],
            'warnings' => [],
            'data' => $listData->items(),
            'dictionaries' =>$this->paging($datapaging),
            'Log' => $this->logging($logging),
        ], 200);
        
    }

    public function pegawaiRequest(Request $request)
    {

        // Action Name For Logging:
        $logging='Access Master Data List';
        
        // Request Data
        $name= $request->input('name');
        $nip= $request->input('nip');
        $size= $request->input('size')  ? $request->input('size') : 10;
        $orderBy = $request->input('orderBy') ? $request->input('orderBy') : 'created_at';
        $sortBy = $request->input('sortBy') ? $request->input('sortBy') : 'ASC';
        $jabatan = $request->input('jabatan');
        $grade = $request->input('grade');
        $status = $request->input('status');


        // Query
        $listData = Pegawai::select('pegawai.*')
        ->where('pegawai.nama','Like','%'.$name.'%')
        ->where('pegawai.nip','Like','%'.$nip.'%')
        ->where('pegawai.jabatan','Like','%'.$jabatan.'%')
        ->where('pegawai.grade','Like','%'.$grade.'%')
        ->where('pegawai.status','Like','%'.$status.'%')
        ->orderBy($orderBy, $sortBy)->paginate($size);
        $datapaging= array($orderBy, $sortBy, $listData);


        return response()->json([
            'success' => true,
            'errors' =>[],
            'warnings' => [],
            'data' => $listData->items(),
            'dictionaries' =>$this->paging($datapaging),
            'Log' => $this->logging($logging),
        ], 200);
        
    }

    public function dokumenRequest(Request $request)
    {

        // Action Name For Logging:
        $logging='Access Master Data List';
        $period = $request->input('period');

        // Query
        $listData = Detail::select('detail.*',  'pegawai.nama', 'pegawai.pangkat', 'pegawai.gol', 'pegawai.norek', 'pegawai.npwp', 'pegawai.jabatan', 'pegawai.grade', 'pegawai.status')
        ->join('pegawai','pegawai.nip','=','detail.nip')
        ->where('detail.periode','=',$period)
        // ->orderBy('grade', 'DESC')
        ->get();


        return response()->json([
            'success' => true,
            'errors' =>[],
            'warnings' => [],
            'data' => $listData,
            'dictionaries' =>[],
            'Log' => $this->logging($logging),
        ], 200);
    }


    public function JumlahPPHRequest(Request $request)
    {

        // Action Name For Logging:
        $logging='Access Master Data List';
        $period = $request->input('period');

        // Query


        $JUMLAH_NOMINAL = Detail::where('periode','=',$period)->get()->sum("tunkir");
        $JUMLAH_POTONGAN = Detail::where('periode','=',$period)->get()->sum("potongan_nilai");
        $JUMLAH_DITERIMA = Detail::where('periode','=',$period)->get()->sum("diterima");
        $JUMLAH_PAJAK = Detail::where('periode','=',$period)->get()->sum("potongan_pph");


        return response()->json([
            'success' => true,
            'errors' =>[],
            'warnings' => [],
            'data' => array(
                'JUMLAH_NOMINAL'=>$JUMLAH_NOMINAL,
                'JUMLAH_POTONGAN'=>$JUMLAH_POTONGAN,
                'JUMLAH_DITERIMA'=>$JUMLAH_DITERIMA,
                'JUMLAH_PAJAK'=>$JUMLAH_PAJAK,
            ),
            'dictionaries' =>[],
            'Log' => $this->logging($logging),
        ], 200);
    }

    public function periodRequest(Request $request)
    {

        // Action Name For Logging:
        $logging='Access Master Data List';
        $period = $request->input('period');

        // Query
        $listData = Period::where('uuid','=',$period)
        ->first();


        return response()->json([
            'success' => true,
            'errors' =>[],
            'warnings' => [],
            'data' => $listData,
            'dictionaries' =>[],
            'Log' => $this->logging($logging),
        ], 200);
    }



    public function listPeriodRequest(Request $request)
    {

        // Action Name For Logging:
        $logging='Access Master Data List';


        return response()->json([
            'success' => true,
            'errors' =>[],
            'warnings' => [],
            'data' => Period::all(),
            'dictionaries' => [],
            'Log' => $this->logging($logging),
        ], 200);
    }


    public function checkRequest(Request $request)
    {

        $nip= $request->input('nip');
        $checkPegawai = Pegawai::where('nip', '=', $nip )->get();

        if(count($checkPegawai)){
            return response()->json([
                'success' => true,
                'errors' =>[],
                'warnings' => [],
                'data' => "ada",
                'dictionaries' => [],
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'errors' =>[],
                'warnings' => [],
                'data' => "kosong",
                'dictionaries' => [],
            ], 200);
        }
        
    }

    


    

    public function postRequest(Request $request)
    {
        $uuid_period= $request->input('uuid_period');
        $end_period= $request->input('end_period');
        $start_period= $request->input('start_period');
        $year= $request->input('year');
        $data= $request->input('data');
       
        // post to period
        $postPeriod = Period::create([
            'uuid'               => $uuid_period,
            'start_period'       => $start_period,
            'end_period'         => $end_period,
            'year'               => $year,
        ]);

        if($postPeriod){
            foreach ($data as $value) {
                $nip = $value['nama'];
                $nip_after_removing_spacing = preg_replace('/\s+/', '', $nip);
                $checkPegawai = Pegawai::where('nip', '=', $nip_after_removing_spacing )->get();

                if(count($checkPegawai)){

                    $postDetail = Detail::create([
                        'uuid'               => Str::uuid()->toString(),
                        'periode'            => $uuid_period,
                        'nip'               => $value['nama'],
                        'tunkir'             => $value['tunkir'],
                        'harikerja'          => $value['harikerja'],
                        'hadir'              => $value['hadir'],
                        'aplha'         => $value['aplha'],
                        'dl'         => $value['dl'],
                        'izin_telat'         => $value['izin_telat'],
                        'izin_pulang'         => $value['izin_pulang'],
                        'cuti_tahun'         => $value['cuti_tahun'],
                        'cuti_sakit'         => $value['cuti_sakit'],
                        'cuti_hamil'         => $value['cuti_hamil'],
                        'cuti_penting'         => $value['cuti_penting'],
                        'cuti_besar'         => $value['cuti_besar'],
                        'cuti_lainnya'         => $value['cuti_lainnya'],
                        'telat_30'         => $value['telat_30'],
                        'telat_60'         => $value['telat_60'],
                        'telat_90a'         => $value['telat_90a'],
                        'telat_90b'         => $value['telat_90b'],
                        'cepat_pulang_30'         => $value['cepat_pulang_30'],
                        'cepat_pulang_60'         => $value['cepat_pulang_60'],
                        'cepat_pulang_90a'         => $value['cepat_pulang_90a'],
                        'cepat_pulang_90b'         => $value['cepat_pulang_90b'],
                        'jurnal_2'         => $value['jurnal_2'],
                        'jurnal_1'         => $value['jurnal_1'],
                        'jurnal_0'         => $value['jurnal_0'],
                        'ganti'         => $value['ganti'],
                        'potongan_persen'         => $value['potongan_persen'],
                        'potongan_nilai'         => $value['potongan_nilai'],
                        'diterima'         => $value['diterima'],
                        'potongan_pph'         => $value['potongan_pph'],
                        
                    ]);
                } else {

                    $postPegawai = Pegawai::create([
                        'uuid'               => Str::uuid()->toString(),
                        'nama'               => $value['onlyname'],
                        'nip'                => $nip_after_removing_spacing,
                        'pangkat'                => "",
                        'gol'                => "",
                        'jabatan'               => $value['jabatan'],
                        'grade'               => $value['grade'],
                        'status'               => $value['status'],
                        'norek'               => "",
                        'npwp'               => "",
                    ]);
                    $postDetail = Detail::create([
                        'uuid'               => Str::uuid()->toString(),
                        'periode'            => $uuid_period,
                        'nip'               => $nip_after_removing_spacing,
                        'tunkir'             => $value['tunkir'],
                        'harikerja'          => $value['harikerja'],
                        'hadir'              => $value['hadir'],
                        'aplha'         => $value['aplha'],
                        'dl'         => $value['dl'],
                        'izin_telat'         => $value['izin_telat'],
                        'izin_pulang'         => $value['izin_pulang'],
                        'cuti_tahun'         => $value['cuti_tahun'],
                        'cuti_sakit'         => $value['cuti_sakit'],
                        'cuti_hamil'         => $value['cuti_hamil'],
                        'cuti_penting'         => $value['cuti_penting'],
                        'cuti_besar'         => $value['cuti_besar'],
                        'cuti_lainnya'         => $value['cuti_lainnya'],
                        'telat_30'         => $value['telat_30'],
                        'telat_60'         => $value['telat_60'],
                        'telat_90a'         => $value['telat_90a'],
                        'telat_90b'         => $value['telat_90b'],
                        'cepat_pulang_30'         => $value['cepat_pulang_30'],
                        'cepat_pulang_60'         => $value['cepat_pulang_60'],
                        'cepat_pulang_90a'         => $value['cepat_pulang_90a'],
                        'cepat_pulang_90b'         => $value['cepat_pulang_90b'],
                        'jurnal_2'         => $value['jurnal_2'],
                        'jurnal_1'         => $value['jurnal_1'],
                        'jurnal_0'         => $value['jurnal_0'],
                        'ganti'         => $value['ganti'],
                        'potongan_persen'         => $value['potongan_persen'],
                        'potongan_nilai'         => $value['potongan_nilai'],
                        'diterima'         => $value['diterima'],
                        'potongan_pph'         => $value['potongan_pph'],
                        
                    ]);
                }
               
            }
            $logging='Create Tunkir Success';
            return response()->json([
                'success' => true,
                'errors' =>[],
                'warnings' => [],
                'data' => [],
                'dictionaries' =>[],
                'Log' => $this->logging($logging),
            ], 200);
        } else {
            $logging='Create Tunkir Error';
            return response()->json([
                'success' => false,
                'errors' =>[],
                'warnings' => [],
                'data' => [],
                'dictionaries' =>[],
                'Log' => $this->logging($logging),
            ], 404);
        }


        
    }

   
}
