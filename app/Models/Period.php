<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    /**
     * @var string
     */
    protected $table = 'period';

    /**
     * @var array
     */
    protected $fillable = [
        'uuid',
        'start_period', 
        'end_period', 
        'year', 
        'updated_at',
        'created_at',
    ];
}

