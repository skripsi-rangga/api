<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    /**
     * @var string
     */
    protected $table = 'pegawai';

    /**
     * @var array
     */
    protected $fillable = [
        'uuid',
        'nama', 
        'nip', 
        'pangkat',
        'gol',
        'jabatan', 
        'grade', 
        'status',
        'norek',
        'npwp',
        'updated_at',
        'created_at',
    ];
}

