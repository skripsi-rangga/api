<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    /**
     * @var string
     */
    protected $table = 'detail';

    /**
     * @var array
     */
    protected $fillable = [
        'uuid',
        'periode', 
        'nip', 
        'tunkir', 
        'harikerja', 
        'hadir',
        'aplha',
        'dl',
        'izin_telat',
        'izin_pulang',
        'cuti_tahun',
        'cuti_sakit',
        'cuti_hamil',
        'cuti_penting',
        'cuti_besar',
        'cuti_lainnya',
        'telat_30',
        'telat_60',
        'telat_90a',
        'telat_90b',
        'cepat_pulang_30',
        'cepat_pulang_60',
        'cepat_pulang_90a',
        'cepat_pulang_90b',
        'jurnal_2',
        'jurnal_1',
        'jurnal_0',
        'ganti',
        'potongan_persen',
        'potongan_nilai',
        'diterima',
        'potongan_pph',
        'updated_at',
        'created_at',
    ];
}

